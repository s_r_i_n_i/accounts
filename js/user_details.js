function user_class(){
    
    
    //=====================ajax call===================//
    
    this.user_details = function(data){
        
         var response_data="";
        
        //==========ajaxcall==========//
        
         ajaxcall.send_data(data,'select_user',function(data) {
            response_data = JSON.parse(data); 
            });
        
         return response_data;
    }
    
    
    //=====================function for validating employee name============================//
  
    this.name_validation = function(data){
        
        if(data.length!=0 && data!=' ')
            return "true";
        else
            return "false";  
        
    }
    
    
    //=====================function for validating account number==========================//
    
    this.acc_no_validation = function(data){
        
        if(data.length==11)
            return "true";
        else
            return "false";
        
    }
    
    
    //=====================function for validating uan number==============================//
    
    
    this.uan_validation = function(data){
        
        if(data.length==12)
            return "true";
        else
            return "false";
        
    }
    
    
  
    //======================function for validating esi number=============================//
    
    
    this.esi_num_validation = function(data){
        
        if(data.length==10)
            return "true";
        else
            return "false";
        
    }
    
    
    //=======================function for validating email address========================//
    
    this.email_validation = function(data){
        
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data))
            return "true";
        else
            return "false";
        
    }
    
    
    //=========================Function for text only input field=========================//
    
    function isnumeric(evt){
        
         var charCode = (evt.which) ? evt.which : evt.keyCode;
            if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
                return true;
                return false;
        
    }
    
  
}


//================================================================= END OF ACCOUNTS CLASS
//=================================================================================================//
    
    


$(document).ready(function() {
    
    
    var user_obj = new user_class();
    
    
    //==========================fetch user names from database ===========//
    
    var my_data = {};
    
    my_data['action'] = "user_names_for_dropdown";
    
    
    var response = user_obj.user_details(my_data);
  
    
    for(i=0; i< response['flag_length'];i++){
        
         var display_name = response[i]['name'];
         var user_hash = response[i]['user_hash'];
        
         $("#user_name").append("<option value="+user_hash+">" + display_name +"</option>"); 
        
    }
     
    //=========================put all the names into the dropdown=========//
        
        $(document).on('click','.item.active.selected',function(){
            
            //===============Enable all the input box after selecting the user name========//
            
            $("#email").attr('disabled',false);
            $("#employee_name").attr('disabled',false);
            $(".radbtn").attr('disabled',false);
            $("#date_of_birth").attr('disabled',false);
            $("#date_of_joining").attr('disabled',false);
            $(".ui.search.fluid.dropdown.designation").removeClass("disabled");
            $("#bank_acc_name").attr('disabled',false);
            $("#axis_bank_acc_no").attr('disabled',false);
            $("#pf_acc_name").attr('disabled',false);
            $("#uan_no").attr('disabled',false);
            $("#esi_record_name").attr('disabled',false);
            $("#esi_insurance_no").attr('disabled',false);

            /*$("#employee_name").val('');
            $("#date_of_birth").val('');
            $("#date_of_joining").val('');
            $("#bank_acc_name").val('');
            $("#axis_bank_acc_no").val('');
            $("#pf_acc_name").val('');
            $("#uan_no").val('');
            $("#esi_record_name").val('');
            $("#esi_insurance_no").val('');
            $("#email").val('');
            $("#designation").dropdown('restore defaults');
            $("#radiobtn1").prop("checked",true);*/
        
            
            //===================Get the user details =============//
        
            var user_hash = $(this).attr('data-value');
            var my_data = {};
            
            my_data['action'] = "get_user_details";
            my_data['user_hash'] = user_hash;


            var response = user_obj.user_details(my_data);
            var EmployeeName = response['employee_name'];
            var Emailid = response['email_id'];
            var Gender = response['gender'];
            var Date_of_birth = response['date_of_birth'];
            console.log(Date_of_birth);
            var Date_of_Joining = response['date_of_joining'];
            var Designation = response['designation'];
            var Bank_acc_name = response['bank_acc_name'];
            var Axis_acc_number = response['axis_acc_no'];
            var Pf_acc_name = response['pf_acc_name'];
            var Uan_number = response['uan_number'];
            var Esi_record_name = response['esi_record_name'];
            var Esi_insurance_number = response['esi_insurance_no'];

           
            //============put the user details into the text boxes ========//
            
            $("#employee_name").val(EmployeeName);

            if(Gender=="Male")
            $("#radiobtn1").prop("checked",true);
            else if(Gender=="Female")
            $("#radiobtn2").prop("checked",true);

            $("#email").val(Emailid);

            $("#date_of_birth").val(Date_of_birth);
            $("#date_of_joining").val(Date_of_Joining);

            $("#bank_acc_name").val(Bank_acc_name);
            $("#axis_bank_acc_no").val(Axis_acc_number);
            $("#pf_acc_name").val(Pf_acc_name);
            $("#uan_no").val(Uan_number);
            $("#esi_record_name").val(Esi_record_name);
            $("#esi_insurance_no").val(Esi_insurance_number);

            $(".ui.search.fluid.dropdown").dropdown('set selected', Designation);
            $(".ui.search.fluid.dropdown").dropdown({
                                    allowLabels:true
                            });

    });
    
    
    /*$("#update_details").on('click', function(){
        
    var employee_name_valid = $("#employee_name").val();
    var result = user_obj.name_validation(employee_name_valid);
    var empnamevalid;
    if(result=="true")
        empnamevalid = true;
    else
        empnamevalid = false;
        
    var name = employee_name_valid.trim();
        
    //===================================Gender_Validation=======================//    
        
    var gender = $("input[name='radiobtn']:checked").val(); 
        
    console.log(gender);
        
    //====================================Date of birth-Validation===============//    
    
    var dob = $("#date_of_birth").val();
        
    //====================================Date of Joining-Validation============//    
        
    var doj = $("#date_of_joining").val();
        
    //=================================Designation-Validation====================//    
        
    var designation = $('#designation option:selected').val();
    
    console.log(designation);
        
    //================================= Bank account name-Validation===========//
        
    var acc_name = $("#bank_acc_name").val();
    var result = user_obj.name_validation(acc_name);
    var accname_valid;
    if(result=="true")
        accname_valid=true;
    else
        accname_valid=false;
        
    //================================== Account number-Validation==============//    
        
    var axis_acc = $("#axis_bank_acc_no").val();
    var result = user_obj.acc_no_validation(axis_acc);
    var axisaccno_valid;
    if(result=="true")
        axisaccno_valid=true;
    else
        axisaccno_valid=false;
        
    //=================================Pf name-Validation======================//    
        
    var pf_name = $("#pf_acc_name").val();
    var result = user_obj.name_validation(pf_name);
    var pfname_valid;
    if(result=="true")
        pfname_valid=true;
    else
        pfname_valid=false;
        
    //==============================Uan number-Validation===================//
    var uan = $("#uan_no").val();
    var result = user_obj.uan_validation(uan);
    var uanno_valid;
    if(result=="true")
        uanno_valid=true;
    else
        uanno_valid=false;
        
    //==============================Esi name-Validation=======================//    
    var esi_name = $("#esi_record_name").val();
    var result = user_obj.name_validation(esi_name);
    var esiname_valid;
    if(result=="true")
        esiname_valid=true;
    else
        esiname_valid=false;
        
    //=============================Esi number-Validation====================//    
    var esi_num = $("#esi_insurance_no").val();
    var result = user_obj.esi_num_validation(esi_num);
    var esinum_valid;
    if(result=="true")
        esinum_valid=true;
    else
        esinum_valid=false;
        
    //============================Email-Validation=========================//
        
    var email = $("#email").val();    
    var result = user_obj.email_validation(email);
    var email_valid;
    if(result=="true")
        email_valid=true;
    else
        email_valid=false;
        
        
    console.log(empnamevalid); 
    console.log(accname_valid); 
    console.log(axisaccno_valid); 
    console.log(pfname_valid);
    console.log(uanno_valid); 
    console.log(esiname_valid); 
    console.log(esinum_valid); 
    console.log(email_valid); 
    console.log(name); 
    console.log(gender); 
    console.log(dob); 
    console.log(doj); 
    console.log(designation); 
    console.log(acc_name); 
    console.log(axis_acc); 
    console.log(pf_name); 
    console.log(uan); 
    console.log(esi_name); 
    console.log(esi_num); 
    console.log(email); 
        
        
        
        
        
        
        
    if(empnamevalid==true && accname_valid==true && axisaccno_valid==true && pfname_valid==true && uanno_valid==true && esiname_valid==true && esinum_valid==true && email_valid==true && name && gender && dob && doj && designation && acc_name && axis_acc && pf_name && uan && esi_name && esi_num && email){
    
    var my_data = {};
    var user_hash = $("#user_name").val();
    console.log(user_hash);
    my_data['action'] = "update_user_details";
    my_data['name'] = name;
    my_data['gender'] = gender;
    my_data['dob'] = dob;
    my_data['doj'] = doj;
    my_data['designation'] = designation;
    my_data['acc_name'] = acc_name;
    my_data['axis_acc'] = axis_acc;
    my_data['pf_acc_name'] = pf_name;
    my_data['uan_no'] = uan;
    my_data['esi_record_name'] = esi_name;
    my_data['esi_insurance_no'] = esi_num;
    my_data['email'] = email;
    my_data['user_hash'] = user_hash;
    var response = user_obj.user_details(my_data); 
    if(response=="Update Successfull...")
        {
    $("#remove_span").html("<span id='result'>Updation Successfull...</span>");
    $("#result").addClass("success");
        
    setTimeout(function() {
        $('#result').remove();
        }, 4000);
        }
        else if(response == "Update not successfull...")
            {
               $("#remove_span").html("<span id='result'>Updation not successfull</span>");
        $("#result").addClass("error");
        setTimeout(function() {
        $('#result').remove();
        }, 4000);
            }
    }
        else
            console.log("vera prachana da");
        
        
    });*/
    
    
    
    //====================Key up events for updating user details ============//
    
    
    //===================update employee name=================//
    
    
    $("#employee_name").on('keyup', function(){
        
        var employee_name = $('#employee_name').val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['employee_name'] = employee_name;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee name";
        var response = user_obj.user_details(my_data); 
        console.log(response);
        
    });
    
    
    //====================update employee email id==============//
    
    
    $("#email").on('keyup', function(){
        
        var email = $("#email").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['email'] = email;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee email-id";
        var response = user_obj.user_details(my_data); 
        console.log(response);
        
    });
    
    
    //====================update employee gender===============//
    
    
    $(".radbtn").on('click', function(){
        var gender = $("input[name='radiobtn']:checked").val();
        console.log(gender);
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['gender'] = gender;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee gender";
        var response = user_obj.user_details(my_data); 
        console.log(response);
        
    });
    
    
    //=====================update employee date_of_birth=========//
    
    $("#date_of_birth").on('change', function(){
        var date_of_birth = $("#date_of_birth").val();
        //console.log(date_of_birth);
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['date_of_birth'] = date_of_birth;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee date_of_birth";
        var response = user_obj.user_details(my_data); 
    });
    
    
    //=======================//=====================update employee date_of_joining=========//
    
      $("#date_of_joining").on('change', function(){
        var date_of_joining = $("#date_of_joining").val();
       // console.log(date_of_joining);
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['date_of_joining'] = date_of_joining;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee date_of_joining";
        var response = user_obj.user_details(my_data); 
          
    });
    
    
    //=====================update employee designation=========//
    
    
       $("#designation").on('change', function(){
           var designation = $('#designation option:selected').val();
         var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['designation'] = designation;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee designation";
        var response = user_obj.user_details(my_data); 
       });
    
    
    
    //=====================update employee bank account number=========//
    
    
    $("#bank_acc_name").on('keyup', function(){
        
        var bank_acc_name = $("#bank_acc_name").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['bank_acc_name'] = bank_acc_name;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee bank_acc_name";
        var response = user_obj.user_details(my_data); 
    });
    
    
    //=====================update employee axis bank account number=========//
    
    $("#axis_bank_acc_no").on('keyup', function(){
        var axis_bank_acc_no = $("#axis_bank_acc_no").val();
        var user_hash = $("#user_name").val();
      
        var my_data = {};
        my_data['axis_bank_acc_no'] = axis_bank_acc_no;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee axis_bank_acc_no";
        var response = user_obj.user_details(my_data);
    });
    
    
    //=====================update employee pf name=========//
    
    
     $("#pf_acc_name").on('keyup', function(){
        
        var pf_acc_name = $("#pf_acc_name").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['pf_acc_name'] = pf_acc_name;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee pf_acc_name";
        var response = user_obj.user_details(my_data);
    })
    
    
    //=====================update employee uan number=========//
    
    
    $("#uan_no").on('keyup', function(){
        
        var uan_no = $("#uan_no").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['uan_no'] = uan_no;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee uan_no";
        var response = user_obj.user_details(my_data);
    })
    
    
    //=====================update employee esi record name=========//
    
    
    $("#esi_record_name").on('keyup', function(){
        
        var esi_record_name = $("#esi_record_name").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['esi_record_name'] = esi_record_name;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee esi_record_name";
        var response = user_obj.user_details(my_data);
    })
    
    
    //=====================update employee esi insurance number=========//
    
    
    $("#esi_insurance_no").on('keyup', function(){
        
        var esi_insurance_no = $("#esi_insurance_no").val();
        var user_hash = $("#user_name").val();
        var my_data = {};
        my_data['esi_insurance_no'] = esi_insurance_no;
        my_data['user_hash'] = user_hash;
        my_data['action'] = "update employee esi_insurance_no";
        var response = user_obj.user_details(my_data);
    })
     
    });


//===============================End of document.ready function==============//
    
    

    