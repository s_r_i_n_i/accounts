//============================= ACCOUNTS _CLASS ============================================//


function accounts_class(){
    
    
    this.check = 0;
    
    this.onload = function(){
        
        $("#email_help").hide();
        $("#employeenamehelp").hide();
        $("#bank_acc_name_help").hide();
        $("#axis_bank_acc_no_help").hide();
        $("#pf_acc_name_help").hide();
        $("#uan_no_help").hide();
        $("#esi_record_name_help").hide();
        $("#esi_insurance_no_help").hide();
    }
    
    //=====================function for validating employee name============================//
  
    this.name_validation = function(data){
        if(data.length!=0 && data!=' ')
            return "true";
        else
            return "false";     
    }
    
    
    //=====================function for validating account number==========================//
    
    this.acc_no_validation = function(data){
        if(data.length==11)
            return "true";
        else
            return "false";
    }
    
    
    //=====================function for validating uan number==============================//
    
    
    this.uan_validation = function(data){
        if(data.length==12)
            return "true";
        else
            return "false";
    }
    
    
  
    //======================function for validating esi number=============================//
    
    
    this.esi_num_validation = function(data){
        if(data.length==10)
            return "true";
        else
            return "false";
    }
    
    
    //=======================function for validating email address========================//
    
    this.email_validation = function(data){
        
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data))
            return "true";
        else
            return "false";
        
    }
    
    
    //=========================Function for text only input field=========================//
    
    function isnumeric(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode;
            if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
                return true;
                return false;
        }
    
  
}


//================================================================= END OF ACCOUNTS CLASS
//=================================================================================================//
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


$(document).ready(function() {
    
    var accounts_obj = new accounts_class();
    

    //=======================Onload function======================//
    
    
    
    accounts_obj.onload();
    
    //========================Employee Name Validation================//
    
    
    
    $("#employee_name").on('input', function(){
        
        $("#employee_name").css({"border":"initial"});
        
        var name = $("#employee_name").val();
        var result = accounts_obj.name_validation(name);
        
        if(result=="true")
            {
              $("#employeenamehelp").hide();
            }   
        else
            {
              $("#employeenamehelp").show();
            }
    });
    
    
    //============================ Bank Account Name Validation =====================//
    
    $("#bank_acc_name").on('input', function(){
        
        $("#bank_acc_name").css({"border":"initial"});
        
        var acc_name = $("#bank_acc_name").val();
        var result = accounts_obj.name_validation(acc_name);
        
        if(result == "true")
        {
            $("#bank_acc_name_help").hide();
        }
        else
        {
            $("#bank_acc_name_help").show();
        }
        
    });
    
    
    //============================= Axis account number validation ================// 
    
    
    $("#axis_bank_acc_no").on('input', function(){
        
        $("#axis_bank_acc_no").css({"border":"initial"});
        
        var axis_acc = $("#axis_bank_acc_no").val();
        var result = accounts_obj.acc_no_validation(axis_acc);
        
        if(result=="true")
        {
            $("#axis_bank_acc_no_help").hide();
        }
        else
        { 
            $("#axis_bank_acc_no_help").show();
        }
        
    });
    
    
    //=================================PF name validation =====================//
    
    $("#pf_acc_name").on('input', function(){
        
        $("#pf_acc_name").css({"border":"initial"});
        
        var pf_name = $("#pf_acc_name").val();
        var result = accounts_obj.name_validation(pf_name);
        if(result == "true")
        {
            $("#pf_acc_name_help").hide();
        }
        else
        {
            $("#pf_acc_name_help").show();
        }
        
    });
    
    //===============================UAN validation ============================//
    
    $("#uan_no").on('input', function(){
        
        $("#uan_no").css({"border":"initial"});
        
        var uan = $("#uan_no").val();
        var result =accounts_obj.uan_validation(uan);
        
        if(result == "true")
        {
            $("#uan_no_help").hide();
        }
        else 
        {
            $("#uan_no_help").show();
        }
        
    });
    
    
    //===============================ESI name Validation =======================//

    $("#esi_record_name").on('input', function(){
        
        $("#esi_record_name").css({"border":"initial"});
        
        var esi_name = $("#esi_record_name").val();
        var result = accounts_obj.name_validation(esi_name);
        
        if(result == "true")
        {
            $("#esi_record_name_help").hide();
        }
        else
        {
            $("#esi_record_name_help").show();
        }
        
    });
    
    
    //==============================ESI number validation =====================//
    
    $("#esi_insurance_no").on('input', function(){
        
        $("#esi_insurance_no").css({"border":"initial"});
        var esi_num = $("#esi_insurance_no").val();
        var result = accounts_obj.esi_num_validation(esi_num);
        if(result == "true")
        {
            $("#esi_insurance_no_help").hide();
        }
        else 
        {
            $("#esi_insurance_no_help").show();
        }
        
    });
    
    
    //========================== Email validation ==============================//
    
    $("#email").on('input', function(){
        
        $("#email").css({"border":"initial"});
        
        var email = $("#email").val();
        var result = accounts_obj.email_validation(email);
        
        if(result == "true")
        {     
            $("#email_help").hide();
        }
        else 
        {   
            $("#email_help").show();
        }
        
     });
    
    //==============================Date of birth onclick event==================//
    
    $("#date_of_birth").on('click', function(){
        
        $("#date_of_birth").css({"border":""});
        
    });
    
    //==============================Date of joining onclick event================//
    
     $("#date_of_joining").on('click', function(){
         
        $("#date_of_joining").css({"border":""});
         
    });
    
    //==============================Designation onclick event====================//
    
    $("#designation").on('change',  function(){
        
        $(".ui.dropdown").css({"border":""});
        
    });
    
    
     //========================== Key Press Events ============================//
    
    
     //========================key press event for axis bank account number length validation===================//
    
    
     $("#axis_bank_acc_no").on('keypress',  function(event){
     var acc_no = $("#axis_bank_acc_no").val();
     var acc_length = acc_no.length;
     if(acc_length>10){
          return false;
     }
     }); 
    
    
    //=========================Key press event for uan number validation==============//
    
    
     $("#uan_no").on('keypress', function(event){
     var uanno = $("#uan_no").val();
     var uanno_length = uanno.length;
     if(uanno_length>11){
         return false;
     }
     });    
    
 
     $("#esi_insurance_no").on('keypress', function(event){  
     var esino = $('#esi_insurance_no').val();
     var esino_length = esino.length;
     if(esino_length>9)
         {
             return false;
         }
         
     });  
      
    $("#employee_name").on('keypress', function(event)
       {
        var charCode = (event.which) ? event.which : event.keyCode;
        if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
        return true;
        return false;
        });
    
    $("#bank_acc_name").on('keypress', function(event)
    {
        var charCode = (event.which) ? event.which : event.keyCode;
        if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
        return true;
        return false;
        });
    
    $("#pf_acc_name").on('keypress', function(event)
    {
        var charCode = (event.which) ? event.which : event.keyCode;
        if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
        return true;
        return false;
        });
    
      $("#esi_record_name").on('keypress', function(event)
    {
        var charCode = (event.which) ? event.which : event.keyCode;
        if(charCode != 46 && charCode > 31 && (charCode < 48 || charCode >57))
        return true;
        return false;
        });
    
    
    //================================= End of Key press Events ========================//
    
    //================================= Submit Button Event =======================//
    

    $('#Add_details').on('click',function(){
        
    //==================================Name-Validation========================//    
        
    var employee_name_valid = $("#employee_name").val();
    var result = accounts_obj.name_validation(employee_name_valid);
    var empnamevalid;
    if(result=="true")
        empnamevalid = true;
    else
        empnamevalid = false;
        
    var name = employee_name_valid.trim();
        
    //===================================Gender_Validation=======================//    
        
    var gender = $("input[name='radiobtn']:checked").val(); 
        
    //====================================Date of birth-Validation===============//    
    
    var dob = $("#date_of_birth").val();
        
    //====================================Date of Joining-Validation============//    
        
    var doj = $("#date_of_joining").val();
        
    //=================================Designation-Validation====================//    
        
    var designation = $('#designation option:selected').val();
        
    //================================= Bank account name-Validation===========//
        
    var acc_name = $("#bank_acc_name").val();
    var result = accounts_obj.name_validation(acc_name);
    var accname_valid;
    if(result=="true")
        accname_valid=true;
    else
        accname_valid=false;
        
    //================================== Account number-Validation==============//    
        
    var axis_acc = $("#axis_bank_acc_no").val();
    var result = accounts_obj.acc_no_validation(axis_acc);
    var axisaccno_valid;
    if(result=="true")
        axisaccno_valid=true;
    else
        axisaccno_valid=false;
        
    //=================================Pf name-Validation======================//    
        
    var pf_name = $("#pf_acc_name").val();
    var result = accounts_obj.name_validation(pf_name);
    var pfname_valid;
    if(result=="true")
        pfname_valid=true;
    else
        pfname_valid=false;
        
    //==============================Uan number-Validation===================//
    var uan = $("#uan_no").val();
    var result = accounts_obj.uan_validation(uan);
    var uanno_valid;
    if(result=="true")
        uanno_valid=true;
    else
        uanno_valid=false;
        
    //==============================Esi name-Validation=======================//    
    var esi_name = $("#esi_record_name").val();
    var result = accounts_obj.name_validation(esi_name);
    var esiname_valid;
    if(result=="true")
        esiname_valid=true;
    else
        esiname_valid=false;
        
    //=============================Esi number-Validation====================//    
    var esi_num = $("#esi_insurance_no").val();
    var result = accounts_obj.esi_num_validation(esi_num);
    var esinum_valid;
    if(result=="true")
        esinum_valid=true;
    else
        esinum_valid=false;
        
    //============================Email-Validation=========================//
        
    var email = $("#email").val();    
    var result = accounts_obj.email_validation(email);
    var email_valid;
    if(result=="true")
        email_valid=true;
    else
        email_valid=false;
        
    if(empnamevalid==true && accname_valid==true && axisaccno_valid==true && pfname_valid==true && uanno_valid==true && esiname_valid==true && esinum_valid==true && email_valid==true && name && gender && dob && doj && designation && acc_name && axis_acc && pf_name && uan && esi_name && esi_num && email){
    
    var my_data = {};
    my_data['name'] = name;
    my_data['gender'] = gender;
    my_data['dob'] = dob;
    my_data['doj'] = doj;
    my_data['designation'] = designation;
    my_data['acc_name'] = acc_name;
    my_data['axis_acc'] = axis_acc;
    my_data['pf_acc_name'] = pf_name;
    my_data['uan_no'] = uan;
    my_data['esi_record_name'] = esi_name;
    my_data['esi_insurance_no'] = esi_num;
    my_data['email'] = email;
    var response_data = "";
    ajaxcall.send_data(my_data,'account_insert',function(data) {
            response_data = data; 
            });
   
    if(response_data=="Insertion Successfull...")
    {
        
    $("#remove_span").html("<span id='result'>Insertion Successfull...</span>");
    $("#result").addClass("success");
        
    setTimeout(function() {
        $('#result').remove();
        }, 4000);
        
    $("#employee_name").val('');
    $("#date_of_birth").val('');
    $("#date_of_joining").val('');
    $("#bank_acc_name").val('');
    $("#axis_bank_acc_no").val('');
    $("#pf_acc_name").val('');
    $("#uan_no").val('');
    $("#esi_record_name").val('');
    $("#esi_insurance_no").val('');
    $("#email").val('');
    $("#designation").dropdown('restore defaults');
    $("#radiobtn1").prop("checked",true);
    }
    else if(response_data=="Insertion not successfull")
        {
        $("#remove_span").html("<span id='result'>Insertion not successfull</span>");
        $("#result").addClass("error");
        setTimeout(function() {
        $('#result').remove();
        }, 4000);
        }
    else{
        $("#remove_span").html("<span id='result'>Email id already exist</span>");
        $("#result").addClass("error");
        setTimeout(function() {
        $('#result').remove();
        }, 4000); 
        }
    }
    
    else 
        {
            
        if(!$("#email").val()){
        $("#email").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#email").focus();
        accounts_obj.check = 1;
        }}    
         
        if(!$("#employee_name").val()){
        $("#employee_name").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#employee_name").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#date_of_birth").val()){
        $("#date_of_birth").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#date_of_birth").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#date_of_joining").val()){
        $("#date_of_joining").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#date_of_joining").focus();
        accounts_obj.check = 1;
        }}
            
        if($("#designation").val()==""){
        $('.ui.dropdown').css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $(".ui.dropdown").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#bank_acc_name").val()){
        $("#bank_acc_name").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#bank_acc_name").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#axis_bank_acc_no").val()){
        $("#axis_bank_acc_no").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#axis_bank_acc_no").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#pf_acc_name").val()){
        $("#pf_acc_name").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#pf_acc_name").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#uan_no").val()){
        $("#uan_no").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#uan_no").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#esi_record_name").val()){
        $("#esi_record_name").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#esi_record_name").focus();
        accounts_obj.check = 1;
        }}
            
        if(!$("#esi_insurance_no").val()){
        $("#esi_insurance_no").css({"border":"1px solid red"});
        if(accounts_obj.check==0){
        $("#esi_insurance_no").focus();
        accounts_obj.check = 1;
        }}
            
        
            
        
        $("#remove_span").html("<span id='result'>Please fill the above details</span>");
        $("#result").addClass("error");
        setTimeout(function() {
        $('#result').remove();
        }, 4000);
        }
    
    });
    
    //==================================End of submit button event======================// 
    
    $('#date_of_birth').datepicker('setDate','01-01-1990');
    $("#date_of_birth").val('');
    
    $('#date_of_joining').datepicker({
    });
    
    $(".ui.fluid.dropdown").dropdown({
                
    allowLabels:true,
    allowReselection:true,
    forceSelection:false
           
    });
});