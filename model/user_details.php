<?php
ini_set('display_errors', 1);

//===============importing db file============// 

include('header.php');

//============== receive sent data==========//

$result = json_decode($_POST['myData']);

//============== establishing db connection======//

$con_obj=new dbcon();   

$connect_ref=$con_obj->connect();



$action = $result->action;

$response = array();

$accounts_edit_object = new accounts_edit_class();


if($action=="user_names_for_dropdown"){
$response = $accounts_edit_object->user_names_for_dropdown($result,$connect_ref);
}

else if($action=="get_user_details"){
$response = $accounts_edit_object->get_user_details($result,$connect_ref);
}

/*else if($action=="update_user_details"){
    $response = $accounts_edit_object->update_user_details($result,$connect_ref);
}*/

else if($action == "update employee name"){
    $response = $accounts_edit_object->update_employee_name($result,$connect_ref);
}

else if($action == "update employee email-id"){
$response = $accounts_edit_object->update_employee_email_id($result,$connect_ref);
}

else if($action == "update employee gender"){
$response = $accounts_edit_object->update_employee_gender($result,$connect_ref);
}

else if($action == "update employee date_of_birth"){
$response = $accounts_edit_object->update_employee_date_of_birth($result,$connect_ref);
}

else if($action == "update employee date_of_joining"){
$response = $accounts_edit_object->update_employee_date_of_joining($result,$connect_ref);
}

else if($action == "update employee bank_acc_name"){
$response = $accounts_edit_object->update_employee_bank_acc_name($result,$connect_ref);
}

else if($action == "update employee axis_bank_acc_no"){
$response = $accounts_edit_object->update_employee_axis_bank_acc_no($result,$connect_ref);
}

else if($action == "update employee pf_acc_name"){
$response = $accounts_edit_object->update_employee_pf_acc_name($result,$connect_ref);
}

else if($action == "update employee uan_no"){
$response = $accounts_edit_object->update_employee_uan_no($result,$connect_ref);
}

else if($action == "update employee esi_record_name"){ 
$response = $accounts_edit_object->update_employee_esi_record_name($result,$connect_ref); 
}

else if($action == "update employee esi_insurance_no"){
$response = $accounts_edit_object->update_employee_esi_insurance_no($result,$connect_ref);  
}

else if($action == "update employee designation"){
$response = $accounts_edit_object->update_employee_designation($result,$connect_ref);  
}


echo JSON_encode($response);


//==============================Account edit class=======================//

class accounts_edit_class{
    
    
    
    //=======================user name fetch function=====================//
    
    
   function user_names_for_dropdown($result,$connect_ref){
        
        
   
        $sql = "select display_name,user_hash from user_details where status=1";
        
         
        $flag=0;
        
        if($stmt = $connect_ref -> prepare($sql)) {
            
            $stmt -> bind_result($user_name,$user_hash);
            $stmt -> execute();
            
            
            while($stmt -> fetch()){
            $response[$flag]['name']= $user_name;
            $response[$flag]['user_hash']= $user_hash;
            $flag++;
            }
            
            
            $response['flag_length'] = $flag;
            $stmt -> close();
            
        }
       
            else{
                
            $response = "error";
                
            }
        
            return $response;   
            
        }
    
    //==========================User details receiving function===========//
    
       function get_user_details($result,$connect_ref){
           
          
           
           
           $sql = "select employee_name,gender,date_of_birth,date_of_joining,designation,bank_acc_name,axis_acc_no,pf_acc_name,uan_number,esi_record_name,esi_insurance_no,email_id from employee_accounts where user_hash=?";
           
           
           $user_hash = $result -> user_hash;
           
           
           
           if($stmt = $connect_ref -> prepare($sql)){
               $stmt -> bind_param("s",$user_hash);
               $stmt -> bind_result($employee_name,$gender,$date_of_birth,$date_of_joining,$designation,$bank_acc_name,$axis_acc_no,$pf_acc_name,$uan_number,$esi_record_name,$esi_insurance_no,$email_id);
               $stmt -> execute();
               
               
               
               $stmt -> fetch();
               
               
                   if($date_of_birth!=NULL && $date_of_joining!=NULL)
                   {
                       
                   $date_of_birth = date('m/d/Y',$date_of_birth);
                   $date_of_joining = date('m/d/Y',$date_of_joining);
                   }
               
                   else
                   {
                       $response = "Date of birth and Date of joining is empty";
                   }
                    
                   $response['employee_name']= $employee_name;
                   $response['gender']= $gender;
                   $response['date_of_birth']= $date_of_birth;
                   $response['date_of_joining']= $date_of_joining;
                   $response['designation']= $designation;
                   $response['bank_acc_name']= $bank_acc_name;
                   $response['axis_acc_no']= $axis_acc_no;
                   $response['pf_acc_name']= $pf_acc_name;
                   $response['uan_number']= $uan_number;
                   $response['esi_record_name']= $esi_record_name;
                   $response['esi_insurance_no']= $esi_insurance_no;
                   $response['email_id']= $email_id;
                       
                                      
               $stmt -> close();
           }
           else
           {
               $response = "error";
           }
           return $response;   
           }     
    
    
    
    
    
    /*function update_user_details($result,$connect_ref){
        
        
        $sql = "update employee_accounts set employee_name=?,gender=?,date_of_birth=?,date_of_joining=?,designation=?,bank_acc_name=?,axis_acc_no=?,pf_acc_name=?,uan_number=?,esi_record_name=?,esi_insurance_no=?,email_id=? where user_hash=?";
        
        
        $user_hash = $result -> user_hash;
        $employee_name = $result -> name;
        $gender = $result -> gender;
        $date_of_birth = strtotime($result -> dob);
        $date_of_joining = strtotime($result -> doj);
        $designation = $result -> designation;
        $bank_acc_name = $result -> acc_name;
        $axis_acc_no = $result -> axis_acc;
        $pf_acc_name = $result -> pf_acc_name;
        $uan_number = $result -> uan_no;
        $esi_record_name = $result -> esi_record_name;
        $esi_insurance_no = $result -> esi_insurance_no;
        $email = $result -> email;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            
            
        $stmt -> bind_param("sssssssssssss",$employee_name,$gender,$date_of_birth,$date_of_joining,$designation,$bank_acc_name,$axis_acc_no,$pf_acc_name,$uan_number,$esi_record_name,$esi_insurance_no,$email,$user_hash);
            
        
            
        $stmt -> execute();
             
             $response = "Update Successfull...";
             $stmt -> close();
            
            
        }
        else
        {
            $response = "Update not successfull...";
        }
        
        return $response;
        
    }*/
    
    
    
    //====================employee name update function====================//
    
    function update_employee_name($result,$connect_ref){
        
        
        $sql = "update employee_accounts set employee_name=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $employee_name = $result -> employee_name;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$employee_name,$user_hash);
            $stmt -> execute();
            $response = "Employee name updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee name updation is not successfull";
        }
        return $response;
        
    }
    
    //=======================employee email-id update function=============//
    
    
    function update_employee_email_id($result,$connect_ref){
        
        
        $sql = "update employee_accounts set email_id=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $email = $result -> email;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$email,$user_hash);
            $stmt -> execute();
            $response = "Employee  email id updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  email id updation is not successfull";
        }
        return $response;
    
    }
    
    //======================employee gender update function==============//
    
    
    function update_employee_gender($result,$connect_ref){
         $sql = "update employee_accounts set gender=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $gender = $result -> gender;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$gender,$user_hash);
            $stmt -> execute();
            $response = "Employee  gender updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  gender updation is not successfull";
        }
        return $response;
    }
    
    
    //======================employee date of birth update function========//
    
    function update_employee_date_of_birth($result,$connect_ref){
         $sql = "update employee_accounts set date_of_birth=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $date_of_birth = strtotime($result -> date_of_birth);
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$date_of_birth,$user_hash);
            $stmt -> execute();
            $response = "Employee  date_of_birth updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  date_of_birth updation is not successfull";
        }
        return $response;
    }
    
    //=======================employee date_of_joining function=========//
    
    
    function update_employee_date_of_joining($result,$connect_ref){
         $sql = "update employee_accounts set date_of_joining=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $date_of_joining = strtotime($result -> date_of_joining);
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$date_of_joining,$user_hash);
            $stmt -> execute();
            $response = $date_of_joining;
            $stmt -> close();
        }
        else{
            $response = "Employee  date_of_joining updation is not successfull";
        }
        return $response;
    }
    
    
    
    //=======================employee bank account name update function======//
    
    
    function update_employee_bank_acc_name($result,$connect_ref){
        
        
        $sql = "update employee_accounts set bank_acc_name=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $bank_acc_name = $result -> bank_acc_name;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$bank_acc_name,$user_hash);
            $stmt -> execute();
            $response = "Employee  bank_acc_name updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  bank_acc_name updation is not successfull";
        }
        return $response;
    
    }
    
    //======================employee axis bank account number update function====//
    
    
     function update_employee_axis_bank_acc_no($result,$connect_ref){
        
        
        $sql = "update employee_accounts set axis_acc_no=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $axis_acc_no = $result -> axis_bank_acc_no;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$axis_acc_no,$user_hash);
            $stmt -> execute();
            $response = "Employee  axis_acc_no updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  axis_acc_no updation is not successfull";
        }
        return $response;
    
    }
    
    //======================employee pf account name update function=========//
    
    
    function update_employee_pf_acc_name($result,$connect_ref){
        
        $sql = "update employee_accounts set pf_acc_name=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $pf_acc_name = $result -> pf_acc_name;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$pf_acc_name,$user_hash);
            $stmt -> execute();
            $response = "Employee  pf_acc_name updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  pf_acc_name updation is not successfull";
        }
        return $response;
        
        
    }
    
    //===================employee uan number update function==============//
    
    
    function update_employee_uan_no($result,$connect_ref){
        
         $sql = "update employee_accounts set uan_number=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $uan_no = $result -> uan_no;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$uan_no,$user_hash);
            $stmt -> execute();
            $response = "Employee  uan_no updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  uan_no updation is not successfull";
        }
        return $response;
    }
    
    
    //========================employee esi record name update function=====//
    
    
    function update_employee_esi_record_name($result,$connect_ref){
         $sql = "update employee_accounts set esi_record_name=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $esi_record_name = $result -> esi_record_name;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$esi_record_name,$user_hash);
            $stmt -> execute();
            $response = "Employee  esi_record_name updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  esi_record_name updation is not successfull";
        }
        return $response;
    }
    
    
    //=====================employee esi insurance number update function=========//
    
    
    function update_employee_esi_insurance_no($result,$connect_ref){
         $sql = "update employee_accounts set esi_insurance_no=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $esi_insurance_no = $result -> esi_insurance_no;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$esi_insurance_no,$user_hash);
            $stmt -> execute();
            $response = "Employee  esi_insurance_no updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  esi_insurance_no updation is not successfull";
        }
        return $response;
    }
    
    
    //======================employee designation update function==========//
    
    function update_employee_designation($result,$connect_ref){
        $sql = "update employee_accounts set designation=? where user_hash=?";
        $user_hash = $result -> user_hash;
        $designation = $result -> designation;
        
        if($stmt = $connect_ref -> prepare($sql)){
            
            $stmt -> bind_param("ss",$designation,$user_hash);
            $stmt -> execute();
            $response = "Employee  designation updation successfull";
            $stmt -> close();
        }
        else{
            $response = "Employee  designation updation is not successfull";
        }
        return $response;
    }
    
   }

?>