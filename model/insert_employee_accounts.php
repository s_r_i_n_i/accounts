<?php
include('header.php');
$result = json_decode($_POST['myData']);
$con_obj=new dbcon();   
$connect_ref=$con_obj->connect();

$response ="";
$prevemail=0;
$account_object = new account_class();


$name = $result -> name ;
$gender = $result -> gender;
$dob = strtotime($result -> dob);
$doj = strtotime($result -> doj);
$designation = $result -> designation;
$acc_name = $result -> acc_name;
$axis_acc = $result -> axis_acc;
$pf_name = $result -> pf_acc_name;
$uan = $result -> uan_no;
$esi_name = $result -> esi_record_name;
$esi_num = $result -> esi_insurance_no;
$email = $result -> email;


$resp = $account_object -> account_insert($name,$gender,$dob,$doj,$designation,$acc_name,$axis_acc,$pf_name,$uan,$esi_name,$esi_num,$email,$connect_ref);
 
echo $resp;

class account_class {
    
    function account_insert($name,$gender,$dob,$doj,$designation,$acc_name,$axis_acc,$pf_name,$uan,$esi_name,$esi_num,$email,$connect_ref)
    {
        
        
        $sql_email_id = "select count(employee_name) from employee_accounts where email_id=?";
        
            
            if($stmt = $connect_ref -> prepare($sql_email_id)) {
            $stmt -> bind_param("s",$email);
            $stmt -> execute();
            $stmt -> bind_result($count);
            $stmt -> fetch();
            $prevemail = $count;
            $stmt -> close();
            }
        if($prevemail>0)
        {
            $response = "Email id already exist";
        }
        else
        {
         
        $sql_user_hash ="select user_hash from user_details where user_email=?";
        if($stmt = $connect_ref -> prepare($sql_user_hash)) {
            $stmt -> bind_param("s",$email);
            $stmt -> execute();
            $stmt -> bind_result($user_hash);
            $stmt -> fetch();
            $stmt->close();
        }
            
            
        $sql = "INSERT INTO employee_accounts (`employee_name`,`gender`,`date_of_birth`, `date_of_joining`, `designation`, `bank_acc_name`,`axis_acc_no`,`pf_acc_name`,`uan_number`,`esi_record_name`,`esi_insurance_no`,`email_id`,`user_hash`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        
       
             if( $stmt = $connect_ref -> prepare($sql) ) {
       
             $stmt -> bind_param("sssssssssssss",$name,$gender,$dob,$doj,$designation,$acc_name,$axis_acc,$pf_name,$uan,$esi_name,$esi_num,$email,$user_hash);
             $stmt -> execute();
             
             $response = "Insertion Successfull...";
             $stmt -> close();
             }
        else
            $response = "Insertion not successfull";
        }
        
        return $response;
        
    }
    
}

